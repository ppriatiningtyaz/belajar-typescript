function upperJS(value) {
    return value.toUpperCase();
}

const resultJS = upperJS('abc');
console.log(resultJS);

const resultAJS = upperJS(123);
console.log(resultAJS);